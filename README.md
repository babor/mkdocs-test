# visnow-mkdocs-test

VisNow test mkdocs+readthedocs


- all docs need to be in a separate tree, no more storing inside of modules
- how do we solve help in plugins? probably we should identify module help by prefix_URL + class
- alternatively we just put explicit help URL in module.xml, which is better? what if we move the docs and URL changes? it could be in properties
- let's assume docs dir will be in a separate path in visnow gitlab project (src/main/resources/docs/)
- internal structure and contents are created manually or via gitlab MD editor
- we need a tool to generate help template from a module:
    - execution: java org.visnow.sth org.visnow.vn.lib.path.to.module /path/to/docs
    - it should create a structure in a given path
    - it should generate module image in images/ dir
    - it should generate index.md with header content generated from module.xml, including module name, description, input/output table and placeholder for further docs
- to successfully move from current docs we need to:
    - export each module help text from Google Docs
    - deal with images:
        - exported MDs will number images from 1 to N
        - write the script to parse current help_desc.html and rename images consequently
        - fix image numbering and copy fixed images (manually? auto?)




