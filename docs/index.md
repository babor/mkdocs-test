# Welcome to VisNow User Guide Test

---

VisNow is a generic visualization framework in Java technology, developed by [visnow.org](https://visnow.org) community since 2020 (2006-2019 designed and developed by the Interdisciplinary Centre for Mathematical and Computational Modelling at University of Warsaw).  
It is a modular data flow driven platform enabling users to create schemes for data visualization, visual analysis, data processing and simple simulations. Motivated by 'Read and Watch' idea, VisNow shows the data as soon and fast as possible giving further opportunity for processing and more in-depth visualization. In a few steps it can create professional images and movies as well as discover unknown information hidden in datasets.  
  
VisNow, not trying to be yet another visualization tool, is targeted at overcoming the drawbacks of other similar software platforms. It is subject to the following keynotes:

*   Clear and legible desktop
*   Network creation support
*   Multifunctional modules
*   Module-Object-Interface connection


