# VisNow data model

---

All VisNow modules operate on data types. Modules have input ports, output ports or both. Each input port needs as an input source a data of some particular type. Similarly each output port produces data of some type.

There are few different data types in VisNow: _field_, _geometry object_ and _basic data types_.

---

## **VisNow field**


Main data type in VisNow is _VisNow field_ (or just _field_).

_VisNow field_ can represent data obtained from physical experiments, simulations and measurements, both time-dependent and static. Data may come from many different areas like fluid dynamics, medical data, weather records, engineering, etc.

_VisNow field examples_

| ![](images/datatypes_meteonoaxes.png) | ![](images/datatypes_footmedical.png) | ![](images/datatypes_vectorfield.png) |
|:-------------------------------------:|:-------------------------------------:|:-------------------------------------:|
| Meteorological                        | Medical (CT)                          | Sample vector field                   |


In general _VisNow field_ is a set of atom elements (geometry) with some particular properties (_components_). There are two main types of _field_:

*   _regular field_ - where atom elements are _points_ on the grid;
*   _irregular field_ - where atom elements are basic geometric figures and bodies called _cells_;

_Regular fields_ can be loaded and stored in files with .vnf extension.

\[Hint: In workspace area:

*   module ports that represent field are colored in light blue, dark blue or violet;
*   to see field details select: right mouse button on field port / ShowContent. This will be called “content window” later on\]

_Sample regular field - atom elements are points_

| ![](./images/datatypes_regularexample.png) | ![](./images/datatypes_regularexamplecontent.png) |
|:------------------------------------------:|:-------------------------------------------------:|
| Sample regular field                       | Corresponding content window                      |


_Sample irregular field - atom elements are triangles_

| ![](./images/datatypes_irregularexample.png) | ![](./images/datatypes_irregularexamplecontent.png) |
|:------------------------------------------:|:-------------------------------------------------:|
| Sample irregular field                       | Corresponding content window                      |


_VisNow fields_ are built like on the charts above:

There are two top-level elements of _field_: geometry and values (otherwise called _components_).

---

### **I. Geometry**

Geometry is representation of _field_ in space (where space can be 2 or 3 dimensional; 2D space is less common but it still appears in some modules e.g., AVS reader, Image reader).

_Field_ is always finite. It consists of finite number of elements and there is need to know what does every single element look like. These elements are described in structure section and their position in the space in coordinates section.

#### Structure

Structure describes atom elements of _field_. There are two different types of _field_: _regular_ and _irregular_. And so there are two different types of structure - _regular_ and _irregular_.

Case 1. Regular

_Field_ with regular structure is composed of regular, finite lattice of points (1-, 2- or 3-dimensional). It can be square lattice generated from orthonormal basis, but it can be also generated from any other basis. Lattice can be also “bended” in a custom way (see coordinates section).

Case 2. Irregular

_Field_ with irregular structure is composed of finite number of _cells_, which are geometric figures and bodies. These can be:

*   0-dimensional (points);
*   1-dimensional (segments);
*   2-dimensional (triangles, quads);
*   3-dimensional (tetrahedrons, pyramids, prisms, hexahedrons).

All these figures and bodies are built on vertices.

Each _cell_ is a part of exactly one _cell set_ which typically describes some particular piece of the model. For example, if one wants to simulate air flow around wing of the airplane, there will be most likely at least two _cell sets_: one for wing and one for air around it, which have different physical properties.

#### Coordinates

Coordinates describe how to arrange atom elements of the _field_ in space.

Case 1. Regular

_Regular field_ is in general a finite lattice of _points_. Coordinates of these _points_ can be defined in two ways:

*   explicitly: where every single _point_ has it's own coordinates defined independently. This allows us to “bend” the lattice. In content window this is called “with explicit coordinates”;
*   generated from basis: coordinates of every single _point_ is calculated from basis vectors (in content window they're called “cell vectors”) and origin vector (in content window it's called “origin”). If origin vector is not defined then \[0,0,0\]T is taken. If basis vectors are not defined then the standard orthonormal basis is taken.

Case 2. Irregular

In this case coordinates are always defined explicitly. So every single _vertex_ has it's own coordinates defined independently. _Vertices_ build _cells_ of _irregular field_ (_vertices_ are called “nodes” in content window).

---

### **II. Values / Components**

Apart from geometry there is also data part which describes state or property of every single element of the _field_. This can be for example, density, energy, speed, momentum, temperature, altitude, wind, etc. So for each _point_ or _cell_ there is a value which describes this element. This can be scalar quantity (e.g., density, temperature) or vector quantity (e.g., momentum, speed). Additionally, property can be of different numeric type (byte, short, int, float, double, complex) logic type (true / false) or text. These properties are called otherwise _components_.

Case 1. Regular

_Regular field_ case is simple. For every _component_ there is one value for one _point_ on the _field_.

Case 2. Irregular

In _irregular field_ each component describes either _vertices_ or _cells_ from some particular _cell set_.

---

### **III. Advanced**

1.  Each _cell_ of _irregular field_ (especially 2-dimensional _cells_) has orientation (front / back side).
2.  Each element of _field_ may have optional _mask_ (visible / invisible).
3.  _VisNow field_ can change over time. Such time-dependent _field_ is organized in _time-frames_ within some arbitrary time range. _Frames_ don't need to be uniformly distributed over time, instead, each _frame_ is described by a single real number which denotes its position on time axis. Only some parts of _field_ can change over time, that is:
    
    *   coordinates of _points_ / _vertices_ (but only in case if they are defined explicitly);
    *   _mask_ (so one can show / hide elements over time);
    *   _values_ / _components_ (change of state / property of the element over time – typical in all time-based simulations).
    
    Data between _frames_ is linearly interpolated. Data outside given time range is taken from the boundary. In content window, time data occurrence is denoted by “time range” and “st.” (abbreviation for “number of steps”).
    
4.  _Regular fields_ support _tiles_. _Tiles_ are typically used when some large data set is split into smaller, separated parts (which usually goes together with separated input files). These parts (_tiles_) are 1-, 2- or 3-dimensional boxes (depends on dimension of the _field_). _Field_ covers one or more _tiles_ (fully or partially).

---

### **IV. Miscellaneous**

*   VisNow calculates automatically basic statistics of _components_ and _boundary_ of irregular field (in content window as “min/max” and “boundary” respectively).
*   _Fields_, _cell sets_, _components_ may have names.
*   “Geometric” and “physical extents” in content window sometimes differs which happens mostly in case of geometrical rescaling of 2D _field_ in 3D space (used mainly in Axes3D module).
*   Coordinates of _points_ / _vertices_ are always real numbers (32 bit float).

---

## **Geometry object**

_Geometry object_ is object passed from any module able to output some graphical content into some viewer module. There are two types of _geometry objects_ 2D and 3D.

\[Hint: In workspace area, module ports that represent geometry object are colored in red for 3D objects and in orange for 2D objects\]

---

## **Basic data types**

_Basic data types_ like integer or real number are present in some modules.

\[Hint: In workspace area, module ports that represent basic data types are colored in light or dark grey\]

---

